#!/bin/bash

## Move configuration file

cp -r ./src/.config ~
cp -r ./src/.xmonad ~
cp ./src/.selected_editor ~
cp ./src/.xsessionrc ~


## Apply change with reboot
sudo reboot
