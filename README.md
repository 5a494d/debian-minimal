# Debian minimal

Debian minimal setup with xmonad and lxterminal

## Install a xorg libraries

```bash
sudo apt install xorg xinit
```

or only required packages

```bash
sudo apt install xserver-xorg-core x11-xserver-utils x11-xkb-utils x11-utils xinit
```

## Install windows manager

```bash
sudo apt install xmonad 

```
### Instal basic packages

```bash
sudo apt install gnupg2 apt-transport-https mc lxterminal pcmanfm git
```

After install basic tools, reboot


### Prepare configuration files

```bash
./setup.sh
```

It's done
